var until               = protractor.ExpectedConditions;
var custLogin           = $('[ng-click="customer()"]');
var btnHome             = $('[ng-click="home()"]');
var lnkDeposit          = $('[ng-click="deposit()"]');
var lnkTransaction      = $('[ng-click="transactions()"]');
var optCustID           = $('[ng-model="custId"]');
var btnLogin            = element(by.css('button[type="submit"]'));
var ownersName          = element(by.css('body > div.ng-scope > div > div.ng-scope > div > div:nth-child(1) > strong > span' ));
var currency            = element(by.css('body > div.ng-scope > div > div.ng-scope > div > div:nth-child(3) > strong:nth-child(3)'));
var optAccountID        = element(by.id('accountSelect'));
var tbxDepositAmount    = element(by.css('[ng-model="amount"]'));
var btnSubmit           = element(by.css('button[type="submit"]'));
var txtSuccessMessage   = element(by.className('error ng-binding'));
var txtTransactionAmt   = element(by.css('#anchor0 > td:nth-child(2)'));


module.exports = {

    clickLogin: async function(id) {
        await browser.wait(until.visibilityOf(btnHome, 2000, "The home button did not load."));
        await custLogin.click();
        await optCustID.click();
        await optCustID.$('[value="' + id + '"]').click();
        await btnLogin.click();
    },

    getAccountOwner: async function() {
        return await ownersName.getText();
    },

    getCurrency: async function() {
        return await currency.getText();
    },

    getSuccessMessage: async function() {
        return await txtSuccessMessage.getText();
    },

    getTransactionAmount: async function() {
        return await txtTransactionAmt.getText();
    },

    changeAccountID: async function() {
        await optAccountID.click();
        await element(by.css('option[label="1011"]')).click();
    },

    clickDepositLink: async function() {
        await lnkDeposit.click();
    },

    clickTransactionLink: async function() {
        await lnkTransaction.click();
    },

    depositAmount: async function(amount) {
        await tbxDepositAmount.sendKeys(amount);
        await btnSubmit.click();
    }

} // end module.exports