var until           = protractor.ExpectedConditions;
var txfUsername     = element(by.id('username'));
var txfPassword     = element(by.id('password'));
//var txtUserDescript = element(by.model(model[options.key]));
var txtUserDescript = element(by.id('formly_1_input_username_0'));
var btnLogin        = $('[ng-click="Auth.login()"]');
//var btnLogin        = $('[ng-click*=Profile]');

module.exports = {

    formLogin: async function(username, password, userDescription) {
        await browser.wait(until.visibilityOf(txfUsername, 5000, 'The username field is not displayed.'));
        await txfUsername.sendKeys(username);
        await txfPassword.sendKeys(password);
        await txtUserDescript.sendKeys(userDescription);
        await btnLogin.click();
    },

    fillForm: async function(username, password, userDescription) {
        await browser.wait(until.visibilityOf(txfUsername, 5000, 'The username field is not displayed.'));
        await txfUsername.sendKeys(username);
        await txfPassword.sendKeys(password);
        await txtUserDescript.sendKeys(userDescription);
    }

} // end module.exports