var until   = protractor.ExpectedConditions;
var txtHome = element(by.css('body > div.jumbotron > div > div > div > h1'));

module.exports = {

    getDisplayTitle: async function() {
        await browser.wait(until.visibilityOf(txtHome, 5000, 'Home text is not displayed'));
        return await txtHome.getText();
    }

}