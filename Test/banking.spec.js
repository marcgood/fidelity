var banking         = require('../Pages/banking.js');
var btnLogOut       = element(by.css('[ng-click="byebye()"]'));
var currencyAmount  = '351';
var invalidAmount   = '72.92';
var HarryPotter     = 2;
var RonWeasly       = 3;
var AlbusDumbledore = 4;

beforeEach( function() {
    browser.get('http://www.way2automation.com/angularjs-protractor/banking/#/login');
});

afterEach( function() {
    btnLogOut.click();     
});


// Test #1 - Customer Login and select Ron Weasly
describe('When bank page loads', function() {
    it('should click to open Ron Weasleys account', async function() {
        await banking.clickLogin(RonWeasly);
        
        expect(await banking.getAccountOwner()).toBe('Ron Weasly');    
    });
});

// Test #2 - change in currency
describe('When account ID is changed', function() {
    it('should change dollar to pound', async function() {
        await banking.clickLogin(AlbusDumbledore);
        await banking.changeAccountID();

        expect(await banking.getCurrency()).toBe('Pound');
    });
});

// Test #3 - Confirm deposit
describe('When a deposit is made', function() {
    it('should return a success message', async function() {
        await banking.clickLogin(HarryPotter);
        await banking.clickDepositLink();
        await banking.depositAmount(currencyAmount);
        await banking.getSuccessMessage();
        
        expect(await banking.getSuccessMessage()).toBe('Deposit Successful');
    });
});

// Test #4 - Review transaction page
describe('When transaction page is viewed', function() {
    it('should contain list of previous transactions', async function() {
        await banking.clickLogin(HarryPotter);
        await banking.clickTransactionLink();

        expect(await banking.getTransactionAmount()).toBe(currencyAmount);
    });
});


