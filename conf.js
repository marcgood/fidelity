var ffBrowser = 'firefox';
var chBrowser = 'chrome';

exports.config = {
    framework:  'jasmine2',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    multiCapabilities: {
        'browserName':  chBrowser
    },
    specs: ['Test/bank*.js'],

    suites: {
        homepage: '',
        registration: '',
        calculator: '',
        banking: '/Test/bank*.js'
    }
}
